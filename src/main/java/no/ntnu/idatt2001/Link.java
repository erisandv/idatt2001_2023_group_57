package no.ntnu.idatt2001;

import java.util.List;

//TODO unit test, javadoc. Ask TA for guide on Junit


/**
 * A Link makes it possible to traverse Passages. It is what binds together the components of
 * a Story.
 *
 * @author Henning Sara Strandå
 */
public class Link {
    private final String text;
    private final String reference;
    private List<Action> actions;

    /**
     * Constructor for a Link
     *
     * @param text A descriptive text indicating a choice or an event in the story. This is what
     *             is visible to the player
     * @param reference A string that unambiguously refers to a Passage in the story. This is what is
     *                  in practice the title of the Passage you want the Link to refer to
     */
    Link(String text, String reference){
        this.text = text;
        this.reference = reference;
    }

    /**
     * Returns the text of the Link
     *
     * @return The text associated with the Link
     */
    public String getText(){
        return text;
    }

    /**
     * Returns the reference to the Passage the links refers to
     *
     * @return The associated Passage as a string
     */
    public String getReference(){
        return reference;
    }

    /**
     * TODO Wait until action is done
     * @param action
     */
    public void addAction(Action action){
        //TODO Shallow copy, should probably be deep

        actions.add(action);
    }

    /**
     * TODO Wait until Action is done
     * @return
     */
    public List<Action> getActions(){
        //TODO Shallow copy, should probably be deep

        return actions;
    }


    /**
     * Returns a textual representation of the Link. Format:
     * Text -> Reference
     * Actions: ##
     *
     * @return A string representing the Link
     */
    @Override
    public String toString() {
        return (text + " -> " + reference + "\nActions: " + actions.size());
    }


    //Passing a generic object here doesn't give any build errors. It's correct... for now...

    /**
     * Tests if an object contains equal values to another object
     * @param o The second object being tested
     * @return A boolean value denoting if the two objects are the same
     */
    @Override
    public boolean equals(Object o){
        if (this == o) return true;
        if (o == null) return false;
        if (this.getClass() != o.getClass()) return false;

        Link link = (Link) o;
        return text.equals(link.getText())
                && reference.equals(link.getReference())
                && actions.equals(link.getActions());
    }

    //TODO does hashcode for Lists work this way (if every element is the same)?

    /**
     * Returns a unique hash representing a particular instance of a Link
     *
     * @return An integer value that acts as a hash for the object
     */
    @Override
    public int hashCode(){
        return text.hashCode() * reference.hashCode() * actions.hashCode();
    }
}
