package no.ntnu.idatt2001;

import java.util.Collection;
import java.util.Map;

//TODO wait until Action is modeled before making unit tests

public class Story {
    private final String title;
    private Map<Link, Passage> passages;
    private final Passage openingPassage;

    Story(String title, Passage openingPassage){
        this.title = title;
        this.openingPassage = openingPassage;
        //TODO shallow?
    }

    public String getTitle() {
        return title;
    }

    public Passage getOpeningPassage() {
        return openingPassage;
    }

    public void addPassage(Passage passage){
        //TODO shallow?
        //Make a Link object with the title and reference equal to the name of the passage
        Link link = new Link(passage.getTitle(), passage.getTitle());
        passages.put(link, passage);
    }

    public Passage getPassage(Link link){
        //TODO shallow?
        return passages.get(link);
    }

    public Collection<Passage> getPassages(){
        //TODO shallow?
        return passages.values();
    }

    @Override
    public boolean equals(Object o){
        if (this == o) return true;
        if (o == null) return false;
        if (this.getClass() != o.getClass()) return false;

        Story story = (Story) o;
        return title.equals(story.getTitle())
                && passages.values().equals(story.getPassages()) //TODO !!
                && openingPassage.equals(story.getOpeningPassage());
    }

    //TODO hashcode
}
