package no.ntnu.idatt2001;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

//Note to self: @nested means its a class within a class. A category of tests is put inside a class
//TODO actions is null so unit tests can not be executed

/**
 * Unit test for the Link class.
 *
 * @author Henning Sara Strandå
 */
public class LinkTest {
    private final Link link = new Link("Test tunnel", "tunnel");

    //Dummy test

    /**
     * Checks if the toString() method actually returns a String object
     */
    @Test
    public void toStringReturnsString(){
        assertNotNull(link.toString());
    }
}
