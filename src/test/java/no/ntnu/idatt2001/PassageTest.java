package no.ntnu.idatt2001;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Unit test for the Passage class
 *
 * @author Henning Sara Strandå
 */
public class PassageTest {
    private final Passage passage1 = new Passage("Opening", "Once upon a time");
    private final Passage passage2 = new Passage("Opening", "Once upon a time");

    /**
     * Check if the toString() method actually returns a String object
     */
    @Test
    public void toStringReturnsString(){ assertNotNull(passage1.toString());}

    //TODO unit test will not work, since links are null (Link also needs an Action)
    @Test
    public void objectsAreEqual(){ assertEquals(passage1, passage2); }

}
