# MiT CashView

This is a Java-based budget management application that allows users to manage their finances by tracking their income and expenses. The application provides users with a simple and intuitive user interface that allows them to easily add, edit, and delete transactions.

## Features

The application includes the following features:

- Adding new expenses
- Edit existing expenses
- Delete expenses
- View a breakdown of expenses by category

## Getting Started

To use this application, you will need to have Java installed on your computer. You can download Java from the [official Java website](https://www.java.com/en/download/).

Once you have Java installed, you can run the application by running the `BudgetManagementApplication.java` file located in the `src` directory.

## Usage

When you first launch the application, you will be presented with the main menu. From here, you can select the option to add a new transaction, view a summary of your income and expenses, or view a breakdown of your expenses by category.

To add a new transaction, select the "Add Transaction" option from the main menu. You will be prompted to enter the transaction details, including the transaction amount, description, category, and date.

To view a summary of your income and expenses, select the "Summary" option from the main menu. This will display a chart showing your total income and expenses for the current month.

To view a breakdown of your expenses by category, select the "Breakdown" option from the main menu. This will display a chart showing your expenses broken down by category for the current month.

To view a history of your transactions, select the "History" option from the main menu. This will display a table showing all of your transactions, sorted by date.

## License

-insert

## Acknowledgments

This project was inspired by [Mint.com](https://www.mint.com/).
